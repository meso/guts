# Copyright 2010 A. R. Akhmerov, C. W. Groth, M. Wimmer.
#
# This file is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.
"""General solver of a continuum transport problem.

Problem is defined as a first order, many component differential equation
I p_x = F(p_y) + V + e, with p_x and p_y longitudinal and transverse momenta,
I --- current operator, F, and V --- arbitrary Hermitian matrices, and e ---
energy (scalar). Current operator conservation is ensured by the fact that
energy enters not multiplied by any matrix. Periodic boundary conditions are
taken in the y-direction. System is assumed to be split into slices, then the
scattering matrix is calculated slice by slice. In the initial stage of
calculation eigenmodes of A is calculated, then matrices F and V are
transformed to this basis and used throughout the calculation.

NB: p_y does not need to be momentum; it may be any discrete quantum number
preserved by free propagation.
"""

from __future__ import division
import numpy as np, numpy.matlib as ml
from scipy import linalg as la, sparse as sp
from guts import sm, misc
from guts.matrix import sigma_z

def find_modes(current):
    """Calculate the transformation to basis where current is sigma_z.

    Returns matrix t such that t.H * m * t transforms matrix m to the desired
    basis.
    """
    assert current.shape[0] == current.shape[1]
    assert misc.how_different(current, current.H) < 1e-13, \
           misc.how_different(current, current.H)

    # eigenvalues of -1 * current are calculated to use the sorting provided by
    # la.eigh function (from smallest to largest eigenvalue)
    (vel, vecs) = la.eigh(-current)
    vecs = np.mat(vecs)
    norm = 1 / np.sqrt(abs(vel))
    t = vecs * np.mat(np.diag(norm))

    return t

def v_to_current(v, t):
    """Transform v to the current basis, specified by a transformation t.

    t is a list of square matrices with total dimension of these matrices
    equal to dimension of v, the mode mixing matrix.
    """
    n = v.shape[0]
    m = len(t)
    assert v.shape == (n, n)
    for i in t:
        assert i.shape[0] == i.shape[1]
        assert i.shape[0] % 2 == 0
    blocks = [m * [None] for i in range(m)]
    for i in enumerate(t):
        blocks[i[0]][i[0]] = sp.csr_matrix(i[1])
    t1 = sp.bmat(blocks, 'csr')
    x = np.array(range(n))
    y = []
    for i in t:
        k, l = len(y) // 2, i.shape[0] // 2
        y += range(k, k + l) + range(n//2 + k, n//2 + k + l)
    y = np.array(y)
    t2 = sp.coo_matrix((np.array(n * [1]), (x, y)), shape=(n, n))
    t1 = t1 * sp.csr_matrix(t2)
    v1 = t1.H * v * t1

    return v1

def smatrix_free(f):
    """Return scattering matrix of the ballistic part of the slice.

    f --- list of l matrices, corresponding to transverse part of kinetic
    energy in the basis where current operator is sigma_z
    """
    l = len(f)

    f_exp = [la.expm(1j * sigma_z(x.shape[0] // 2) * x) for x in f]
    m = sm.MMatrix()
    blocks = [l * [None] for i in range(l)]

    for i in range(l):
        n = f_exp[i].shape[0] // 2
        blocks[i][i] = f_exp[i][: n, : n]
    m.m11 = sp.bmat(blocks).todense()

    for i in range(l):
        n = f_exp[i].shape[0] // 2
        blocks[i][i] = f_exp[i][n :, : n]
    m.m21 = sp.bmat(blocks).todense()

    for i in range(l):
        n = f_exp[i].shape[0] // 2
        blocks[i][i] = f_exp[i][: n, n :]
    m.m12 = sp.bmat(blocks).todense()

    for i in range(l):
        n = f_exp[i].shape[0] // 2
        blocks[i][i] = f_exp[i][n :, n :]
    m.m22 = sp.bmat(blocks).todense()

    return sm.SMatrix.from_m(m)

def smatrix_mixing(v, q=1):
    """Calculate the scattering matrix corresponding to mode mixing

    Mixing matrix v should be written in the current basis.
    q is the order of Pade approximation to be used.
    """
    v1 = np.mat(la.expm(1j * sigma_z(v.shape[0] // 2) * v, q))
    m = sm.MMatrix.from_matrix(v1)
    return sm.SMatrix.from_m(m)
