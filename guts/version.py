# Copyright 2010 A. R. Akhmerov, C. W. Groth, M. Wimmer.
#
# This file is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.
def _get_version_from_git():
    import subprocess, os

    guts_dir = os.path.dirname(__file__)
    try:
        p = subprocess.Popen(['git', 'describe'], cwd=guts_dir,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except OSError:
        return

    if p.wait() != 0:
        return
    version = p.communicate()[0].strip()

    try:
        p = subprocess.Popen(['git', 'diff', '--quiet'], cwd=guts_dir)
    except OSError:
        version += '-confused'  # This should never happen.
    else:
        if p.wait() == 1:
            version += '-dirty'
    return version

version = _get_version_from_git()
if version == None:
    version = 'unknown'
