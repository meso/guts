# Copyright 2010 A. R. Akhmerov, C. W. Groth, M. Wimmer.
#
# This file is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.
from __future__ import division
from guts.misc import *
from scipy import signal
import numpy as np
from operator import add

def test_make_gaussian_disorder():
    # This test is slow because it calculates a lot.
    def make_n(n):
        for i in range(n):
            a = make_gaussian_disorder((size, size), (xi, xi), 1)
            yield signal.correlate2d(a, a, boundary='wrap')

    np.random.seed(1)
    size = 15
    xi = .1
    n = 70
    s = reduce(add, make_n(n)) / n / size**2
    sc = s[3*size//4 : 5*size//4, 3*size//4 : 5*size//4]
    xr, yr = np.meshgrid(range(2*size - 1), range(2*size - 1))
    s1 = (np.exp(-((xr - size + 1)**2 + (yr - size + 1)**2) /
                  (2 * xi**2 * size**2)) /
          (2 * np.pi * xi**2))
    s1c = s1[3*size//4 : 5*size//4, 3*size//4 : 5*size//4]
    assert abs(s1c - sc).max() < s1c.max() * 0.1

    w, l = 60, 72
    xix, xiy = 1/15, 1/28
    np.random.seed(1)
    dis_y = make_gaussian_disorder((w, l), (xix, xiy), 1)
    np.random.seed(1)
    dis_ky = make_gaussian_disorder((w, l), (xix, xiy), 1, False, 0)
    dis_ky2 = np.fft.fftn(dis_y/w, axes=(0, ))
    assert how_different(dis_ky[ : w//2 - 1, :],
                         dis_ky2[ : w//2 - 1, :]) < 1e-15
