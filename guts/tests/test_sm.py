# Copyright 2010 A. R. Akhmerov, C. W. Groth, M. Wimmer.
#
# This file is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.
from __future__ import division
import numpy as np, numpy.matlib as ml
from guts.sm import *
from guts.misc import how_different
from guts.matrix import make_circular_s, make_gaussian_h

def test_conversion():
    """Test MMatrix.from_s and SMatrix.from_m."""
    np.random.seed(13)
    s = make_circular_s(4)
    m = MMatrix.from_s(s)
    s2 = SMatrix.from_m(m)
    for submat in [(s.r, s2.r), (s.t, s2.t),
                   (s.tp, s2.tp), (s.rp, s2.rp)]:
        assert how_different(*submat) < 1e-12, how_different(*submat)

def test_unitarity():
    """Test unitarize() and how_nonunitary()"""
    np.random.seed(10)
    s = make_circular_s(4)
    assert s.how_nonunitary() < 1e-13
    s.r[0, 0] += 1               # Destroy unitarity.
    s.unitarize()
    assert s.how_nonunitary() < 1e-13

def test_combine_gen():
    """Test fast_combine and combine."""
    np.random.seed(10)
    s1 = make_circular_s(4)
    s2 = make_circular_s(4)
    s21 = combine(s2, s1)
    s21_fast = fast_combine(s2, s1)
    s21_min = min_combine(s2, s1)
    assert how_different(s21.t, s21_fast.t) < 1e-15
    assert how_different(s21.r, s21_fast.r) < 1e-15
    assert how_different(s21.r, s21_min.r) < 1e-15

    m1 = MMatrix.from_s(s1)
    m2 = MMatrix.from_s(s2)

    m21 = m2 * m1
    m21_alt = MMatrix.from_s(s21).to_matrix()
    assert how_different(m21.to_matrix(), m21_alt) < 1e-8

def test_mmatrix_mul():
    a = ml.matrix('1 2; 3 4')
    b = ml.matrix('5 6; 7 8')
    m = MMatrix.from_matrix(a)
    n = MMatrix.from_matrix(b)
    assert (a*b == (m*n).to_matrix()).all()

def test_cond_noise():
    def make_some_scattering_matrix(alpha):
        import math
        r = math.cos(alpha)
        t = math.sin(alpha)
        s = np.mat([[ 0,  r,  0, -t],
                    [-r,  0,  t,  0],
                    [ 0, -t,  0, -r],
                    [ t,  0,  r,  0]])
        return SMatrix.from_matrix(s)

    import math
    eps = 1e-14

    cond, noise = make_some_scattering_matrix(0).cond_noise()
    assert -eps < cond < eps
    assert -eps < noise < eps

    cond, noise = make_some_scattering_matrix(math.pi / 4).cond_noise()
    assert abs(cond - 1) < eps
    assert abs(noise - 0.5) < eps

    cond, noise = make_some_scattering_matrix(math.pi / 2).cond_noise()
    assert abs(cond - 2) < eps
    assert -eps <= noise < eps

def test_from_tb():
    """Test MMatrix.from_tb.

    Check current conservation and determinant.
    """
    np.random.seed(10)
    h = make_gaussian_h(4)
    hop = make_gaussian_h(4) + 1j * make_gaussian_h(4)
    m = MMatrix.from_tb(h, hop)
    assert m.how_not_conserving() < 1e-10, m.how_not_conserving()

def test_get_wave_function():
    from numpy.linalg import norm
    np.random.seed(10)
    s1 = make_circular_s(2)
    s2 = make_circular_s(2)
    psi1, psi2 = get_wave_function(s1, s2)
    s3 = min_combine(s1, s2)
    assert abs(2 - norm(s3.r * np.mat([1, 1]).T)**2 -
               norm(psi1)**2 + norm(psi2)**2) < 1e-10

def test_polar():
    """Test polar_decomposition and from_polar methods of SMatrix."""
    np.random.seed(10)
    s = make_circular_s(4)
    dec = s.to_polar()
    s2 = SMatrix.from_polar(*dec)
    for i in dec[0]:
        assert 0 <= i <= 1
    for i in dec[1:]:
        assert how_different(i.H * i, ml.identity(4)) < 1e-13
    assert how_different(s.to_matrix(), s2.to_matrix()) < 1e-13

def test_make_lead_s():
    """Test make_lead_s."""
    np.random.seed(5)
    s = make_circular_s(4)
    slead = make_lead_s(MMatrix.from_s(s))
    assert slead.how_nonunitary() < 1e-14, slead.how_nonunitary()
    assert how_different(abs(combine(slead, s).to_matrix()),
                         abs(slead.to_matrix())) < 1e-13