# Copyright 2010 A. R. Akhmerov, C. W. Groth, M. Wimmer.
#
# This file is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.
"""Generation of various matrices"""

from __future__ import division
import numpy as np
from numpy import random
from scipy import linalg as la, sparse as sp
from guts import sm

def sigma_z(n, sparse=True):
    """Create a sigma_z matrix."""
    sigma_z = sp.csc_matrix(np.diag(n * [1] + n * [-1]))
    if not sparse:
        sigma_z = sigma_z.todense()
    return sigma_z

def sigma_x(n, sparse=True):
    """Create a sigma_x matrix."""
    sigma_x = sp.bmat([[None, sp.identity(n)],
                        [sp.identity(n), None]], 'csc')
    if not sparse:
        sigma_x = sigma_x.todense()
    return sigma_x

def sigma_y(n, sparse=True):
    """Create a sigma_y matrix."""
    sigma_y = sp.bmat([[None, -1j * sp.identity(n)],
                        [1j * sp.identity(n), None]], 'csc')
    if not sparse:
        sigma_y = sigma_y.todense()
    return sigma_y

def isigma_y(n, sparse=True):
    """Create a matrix i * sigma_y.

    Introduced for brevity and to keep variable type real.
    """
    isigma_y = sp.bmat([[None, sp.identity(n)],
                        [-sp.identity(n), None]], 'csc')
    if not sparse:
        isigma_y = isigma_y.todense()
    return isigma_y


def make_gaussian_h(n, sym='A'):
    """Make a n*n random gaussian Hamiltonian.

    sym --- Altland-Zirnbauer symmetry class of the Hamiltonian.

    The representations of symmetry operators were chosen as follows:
    AI: T: H = H^*.
    AII: T: H = sigma_y H^* sigma_y.
    AIII: C: H = -sigma_z H sigma_z.
    BDI: T: H = sigma_z H^* sigma_z, CT: H = -H^*.
    CII: T: H = -sigma_y H^* sigma_y, CT: H = sigma_y tau_z H^* sigma_y tau_z.
    D & DIII: T: H = -sigma_y H^* sigma_y, CT: H = -H^*.
    C & CI: T: H = H^*, CT: H = sigma_y H^* sigma_y.

    The superconducting symmetry classes are written in the Majorana basis.
    """
    assert sym in ['A', 'AI', 'AII', 'AIII', 'BDI', \
                   'CII', 'D', 'DIII', 'C', 'CI']
    if sym == 'A':
        h = np.mat(random.randn(n, n) +
                1j * random.randn(n, n)) / np.sqrt(2)
    elif sym == 'AI':
        h = np.mat(random.randn(n, n))
    elif sym == 'AII':
        h = np.mat(random.randn(n, n) +
                1j * random.randn(n, n)) / np.sqrt(2)
        h2 = h - isigma_y(n // 2) * np.conj(h) * isigma_y(n // 2)
        h = h2/2
    elif sym == 'AIII':
        h = np.mat(random.randn(n, n) +
                1j * random.randn(n, n)) / np.sqrt(2)
        h2 = h - sigma_z(n // 2) * h * sigma_z(n // 2)
        h = h2/2
    elif sym == 'BDI':
        h = 1j * np.mat(random.randn(n, n))
        h2 = h - sigma_z(n // 2) * h * sigma_z(n // 2)
        h = h2/2
    elif sym == 'CII':
        h = np.mat(random.randn(n, n) +
                1j * random.randn(n, n)) / np.sqrt(2)
        h2 = h - isigma_y(n // 2) * np.conj(h) * isigma_y(n // 2)
        h = h2/2
        tau_z = kron(np.identity(n // 2), sigma_z(1, False))
        h2 = h - tau_z * h * tau_z
        h = h2/2
    elif sym == 'D':
        h = 1j * np.mat(random.randn(n, n))
    elif sym == 'DIII':
        h = 1j * np.mat(random.randn(n, n))
        h2 = h - isigma_y(n // 2) * np.conj(h) * isigma_y(n // 2)
        h = h2/2
    elif sym == 'C':
        h = np.mat(random.randn(n, n) +
                1j * random.randn(n, n)) / np.sqrt(2)
        h2 = h + isigma_y(n // 2) * np.conj(h) * isigma_y(n // 2)
        h = h2/2
    elif sym == 'CI':
        h = np.mat(random.randn(n, n))
        h2 = h + isigma_y(n // 2) * np.conj(h) * isigma_y(n // 2)
        h = h2/2
    hf = (h + h.H)/2
    return hf

def make_some_current(n, sym='A'):
    """Make a random nonchiral current operator of order n.

    The only difference from the output of make_gaussian_h is that
    eigenvalues come in pairs of opposite sign.
    """
    cur = make_gaussian_h(2*n, sym)
    (vals, vecs) = la.eigh(cur)
    vals = sigma_z(n) * np.mat(np.diag(abs(vals)))
    vecs = np.mat(vecs)
    cur = vecs * vals * vecs.H
    return cur

def make_circular_s(n, sym='A', charge=None):
    """Make a 2n*2n scattering matrix belonging to a circular ensemble.

    sym --- symmetry class of the scattering matrix,
    charge --- 1D topological charge of the matrix
    Matrices are generated in canonical form, see
    /nano/topological_charge/notes.pdf for definitions of canonical form and
    of topological charge.

    This function relies on the fact that QR decomposition in SciPy returns
    positive real values on the diagonal of R, for more discussion see
    arXiv:math-ph/0609050
    """

    # Generate a random matrix with proper electron-hole symmetry
    # This matrix is a random element of symmetric groups U(N), O(N), or Q(N)
    n *= 2
    if sym in ['A', 'AI', 'AII', 'AIII']:
        h = np.mat(random.randn(n, n) + 1j * random.randn(n, n))
        s =  np.mat(la.qr(h)[0]) * np.exp(2j * np.pi * random.rand())
    elif sym in ['D', 'DIII', 'BDI']:
        h = np.mat(random.randn(n, n))
        s = np.mat(la.qr(h)[0])
        if sym != 'BDI' and charge != None:
            assert charge in [-1, 1]
            det = la.det(s)
            if sym == 'DIII':
                det *= (-1)**(n//4)
            if (charge > 0) != (det > 0):
                if n == 2:
                    s_x = sigma_x(1)
                else:
                    s_x = sp.bmat([[sp.identity(n-2), None],
                                   [None, sigma_x(1)]])
                s *= s_x
    elif sym in ['C', 'CI', 'CII']:
        h = np.mat(random.randn(n, n) + 1j * random.randn(n, n))
        s_y = sp.csr_matrix(kron(np.identity(n//2), sigma_y(1, False)))
        h2 = h + s_y * np.conj(h) * s_y
        s = np.mat(la.qr(h2)[0])

    n //= 2
    # Modify the matrix so that it has proper time-reversal symmetry:
    if sym == 'AI' or sym == 'CI':
        s = s.T * s
    elif sym == 'AII' or sym == 'DIII':
        assert n%2 == 0
        is_y = sp.csr_matrix(kron(np.identity(n), isigma_y(1, False)))
        s = s.T * is_y * s

    # Modify the scattering matrix, so it has proper chiral symmetry:
    if sym in ['AIII', 'BDI', 'CII']:
        if sym != 'CII':
            if charge == None:
                charge = np.random.randint(-n, n + 1)
            else:
                assert -n <= charge <= n and type(charge) is int, charge
            charge += n
            sgn = charge * [-1] + (2*n - charge) * [1]
        else:
            if charge == None:
                charge = np.random.randint(-n//2, n//2 + 1)
            else:
                assert -n//2 <= charge <= n//2 and type(charge) is int, charge
            charge += n//2
            sgn = 2 * charge * [-1] + 2 * (n - charge) * [1]
        diag = sp.lil_diags([sgn], [0], (2*n, 2*n)).tocsr()
        s = s.H * diag * s

    return sm.SMatrix.from_matrix(s)

def make_circular_m(n, sym='A'):
    """Make a 2n*2n transfer matrix belonging to a circular ensemble.

    sym --- symmetry class of the scattering matrix."""
    return sm.MMatrix.from_s(make_circular_s(n, sym))

def kron(*m):
    """Kronecker product of an arbitrary number of arguments."""
    prod = reduce(np.kron, m)
    return prod

def pf(a):
    """Calculate Pfaffian of a real antisymmetric matrix."""
    # If input does not fit, make it real antisymmetric.
    assert abs(a.imag).max() < 1e-10
    assert abs(a + a.T).max() < 1e-10, abs(a + a.T).max()
    a = a.real
    (t, z) = la.schur(a, output='real', overwrite_a=True)
    l = np.diag(t, 1)
    return np.prod(l[::2]) * la.det(z)
