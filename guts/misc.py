# Copyright 2010 A. R. Akhmerov, C. W. Groth, M. Wimmer.
#
# This file is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.
"""Various useful functions"""

from __future__ import division
import numpy as np
from scipy.linalg import norm
from guts import sm
from guts.matrix import sigma_z

def make_gaussian_disorder((w, l), (xiy, xix), a, real_space=True, av=0):
    """Generate a disordered potential landscape with gaussian correlations.

    w --- amount of non-negative frequency fourier components or
    system width
    l --- length of the system
    xix --- correlation length in x direction in units of system length
    xiy --- correlation length in y direction in units of system width
    a --- disorder strength
    av --- disorder average
    real_space --- return real space potential, not fourier transform in
    transverse direction.

    The function is written such that for the same seed and the same (xix, xiy)
    it will generate same disorder configuration.

    The dimensionless disorder strength K0 as defined in arXiv:1002.0817
    is equal to a**2.
    """
    m, n = max(int(1.5/xix),4), max(int(1.5/xiy),4)
    xr, yr = np.meshgrid(range(-m, m + 1), range(n))
    ampl = a * np.exp(- np.pi**2 * (xr**2 * xix**2 + yr**2 * xiy**2))
    dis =  ampl * (np.random.normal(0, 1, ampl.shape) +  1j *
                   np.random.normal(0, 1, ampl.shape)) / np.sqrt(2)
    dis2 = np.zeros((w, l), np.complex)
    l1 = (l - 1)//2
    dis2[ : min(w, n), : min(l1, m) + 1] = dis[ : min(w, n),
                                               m : m + min(l1, m) + 1]
    dis2[1 : min(w, n), -min(l1, m) : ] = dis[1 : min(w, n),
                                              m - min(l1, m) : m]
    dis2[0, -min(l1, m) : ] = \
        (dis[0, m + 1: m + min(l1, m) + 1])[::-1].conjugate()
    dis2[0, : ] /= 2
    dis2[0, 0] = av
    dis2 = l * np.fft.ifft(dis2)
    dis2[0, : ] = dis2[0, : ].real
    if not real_space:
        dis2[0, : ] *= 2
    else:
        dis2 = 2 * w * np.fft.ifftn(dis2[ : w//2, : ],
                                    s=(w, ), axes=(0, )).real
    return dis2

def how_different(a, b, normtype=None):
    """Calculate relative difference of arrays a and b.

    Compute || a - b || / max(|| a ||, || b ||), where the kind of norm is
    specified by parameter `normtype' which will be passed on to
    scipy.linalg.norm. The parameter can take, amongst others, the following
    values:

    None -- Frobenius norm (default)
    1    -- 1-norm
    2    -- 2-norm
    inf  -- infinity-norm

    Consult the documentation of scipy.linalg.norm for all possibilities.
    """
    max_norm = max(norm(a, normtype), norm(b, normtype))
    return norm(a - b, normtype)/max_norm
