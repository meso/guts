# Copyright 2010 A. R. Akhmerov, C. W. Groth, M. Wimmer.
#
# This file is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.
"""Routines for scattering and transfer matrices"""

from __future__ import division
import numpy as np, numpy.matlib as ml
import scipy.linalg as la, scipy.sparse as sp

class SMatrix(object):
    """A scattering matrix.

    The conventions used for labelling the sub matrices are taken from section
    1.1.1 of the doctoral thesis of P. W. Brouwer, "On the Random-Matrix Theory
    of Quantum Transport"
    (http://lorentz.leidenuniv.nl/beenakker/theses/brouwer/brouwer.html).
    """

    def __init__(self):
        self.r = None
        self.t = None
        self.tp = None
        self.rp = None

    @classmethod
    def from_matrix(cls, mat, nx=None, ny=None):
        """Make a SMatrix from a general matrix.

        mat --- matrix or np.array
        nx, ny --- shape of the reflection block of the s-matrix to make.
        Other blocks are calculated such that the shapes agree.
        If not given, a value is guessed according to a typical situation.
        """
        if nx == None and ny == None:
            nx = mat.shape[0] // 2
            ny = nx
        if nx == None:
            nx = ny
        if ny == None:
            ny = nx
        if type(mat) != np.matrix:
            mat = np.mat(mat)

        s = cls()
        s.r = mat[: nx, : ny]
        s.t = mat[nx :, : ny]
        s.tp = mat[: nx, ny :]
        s.rp = mat[nx :, ny :]
        return s

    @classmethod
    def from_m(cls, m):
        """Make a SMatrix from a MMatrix."""
        s = cls()
        # r = -m22^-1 * m21
        # t = m11^H^-1 = m11 - m12 * m22^-1 * m21
        # tp = m22^-1
        # rp = m12 * m22^-1
        s.tp = m.m22.I
        s.r = -s.tp * m.m21
        s.t = m.m11 + m.m12 * s.r
        s.rp = m.m12 * s.tp
        return s

    @classmethod
    def from_polar(cls, tran, u, v, up, vp):
        """Make a SMatrix from its polar decomposition."""
        s = cls()
        tt = np.mat(np.diag(np.sqrt(tran)))
        rr = np.mat(np.diag(np.sqrt(1 - tran)))
        s.tp = u * tt * vp
        s.t = v * tt * up
        s.r = - u * rr * up
        s.rp = v * rr * vp
        return s

    def to_matrix(self):
        return np.bmat([[self.r, self.tp], [self.t, self.rp]])

    def to_polar(self):
        """Calculate the polar decomposition of a SMatrix.

        Return a 5-tuple consisting of the list of transmission eigenvalues
        tran, and 4 unitary matrices U, V, U', V', as defined in
        arXiv:cond-mat/9612179 Eq. 1.24.  The function tries to use all the
        available blocks of the scattering matrix and returns unit matrices
        instead of the elements of decomposition which are unrecoverable.

        This function does not use LU deconmposition for efficiency yet, so it
        may be optimized.
        """
        (u, tran, vp) = la.svd(self.tp)
        tran = tran**2
        (u, vp) = (np.mat(u), np.mat(vp))
        rr = np.mat(np.diag(np.sqrt(1 - tran)))
        v = self.rp * vp.H * rr.I
        up = - rr.I * u.H * self.r

        (tran, u, up, v, vp) = 5 * (None,)
        if self.r != None:
            (u, r, up) = la.svd(self.r)
            ri = 1/r
            tran = 1 - r**2
            n = tran.shape[0]
            ti = 1/np.sqrt(tran)
            (u, up) = (np.mat(-u), np.mat(up))
            tti = sp.csc_matrix(sp.lil_diags([ti], [0], (n, n)))
            rri = sp.csc_matrix(sp.lil_diags([ri], [0], (n, n)))
        if self.tp != None:
            if self.r != None:
                vp = tti * u.H * self.tp
            else:
                (u, t, vp) = la.svd(self.tp)
                tran = t**2
                n = tran.shape[0]
                ti = 1/t
                ri = 1/np.sqrt(1 - tran)
                (u, vp) = (np.mat(u), np.mat(vp))
                tti = sp.csc_matrix(sp.lil_diags([ti], [0], (n, n)))
                rri = sp.csc_matrix(sp.lil_diags([ri], [0], (n, n)))
        if self.rp != None:
            if self.tp != None:
                v =  self.rp * vp.H * rri
            else:
                (v, r, vp) = la.svd(self.rp)
                ri = 1/r
                tran = 1 - r**2
                n = tran.shape[0]
                ti = 1/np.sqrt(tran)
                (v, vp) = (np.mat(v), np.mat(vp))
                tti = sp.csc_matrix(sp.lil_diags([ti], [0], (n, n)))
                rri = sp.csc_matrix(sp.lil_diags([ri], [0], (n, n)))
        if self.t != None:
            if self.r == None and self.rp == None:
                (v, t, up) = la.svd(self.t)
                tran = t**2
                (u, vp) = (np.mat(u), np.mat(vp))
            elif self.r == None and self.rp != None:
                up = tti * v.H * self.t
            elif self.r != None and self.rp == None:
                v = self.t * up.H * tti
            elif self.tp == None:
                up = tti * v.H * self.t
                vp = rri * v.H * self.r

        return tran, u, v, up, vp

    def unitarize(self):
        """Project SMatrix to the closest unitary matrix.

        This function projects the SMatrix to the closest matrix that is
        unitary up to (about) machine precision. It is particularly useful to
        minimize the accumulation of errors after several combinations of
        scattering matrices.

        WARNING: This function makes the SMatrix unitary, regardless of how bad
        the error was before. The projected matrix is the closest unitary
        matrix to the unprojected matrix, but nevertheless the projected matrix
        may be far from the correct matrix that you might want to compute

        Usually, unitarizing if the unitarity error is not too big does not
        introduce a big error in your calculations. However, in doing so, one
        looses a natural check for the calculations, namely checking for
        unitarity. These things have to be kept in mind when using this
        function.

        The function uses the polar decomposition
        (http://en.wikipedia.org/wiki/Polar_decomposition, computed using the
        svd). The polar decomposition is guaranteed mathematically to yield the
        closest unitary matrix (in the matrix norm sense).  See Mathematics
        Magazine 48, 192 (1975).
        """
        U, Sigma, V = la.svd(self.to_matrix())
        U = np.mat(U)           # Necessary as la.svd returns arrays.
        V = np.mat(V)
        mat = U*V

        n = self.r.shape[0]
        self.r = mat[: n, : n]
        self.t = mat[n :, : n]
        self.tp = mat[: n, n :]
        self.rp = mat[n :, n :]

    def how_nonunitary(self, norm=None):
        """Calculate deviation from unitarity.

        Compute || 1 - S^dagger S ||, where the kind of norm is specified by
        parameter `norm' which will be passed on to scipy.linalg.norm.  The
        parameter can take, amongst others, the following values:

        None -- Frobenius norm (default)
        1    -- 1-norm
        2    -- 2-norm
        inf  -- infinity-norm

        Consult the documentation of scipy.linalg.norm for all possibilities.
        """
        n=self.r.shape[0]
        m=self.rp.shape[0]
        return la.norm(ml.identity(n+m, dtype=self.r.dtype)
                       - self.to_matrix().H * self.to_matrix(),
                       norm)

    def cond_noise(self):
        """Calculate conductance and noise.

        Obsolete, usage of cond(s) and noise(s) is preferable.
        """
        return self.cond(), self.noise()

    def cond(self):
        """Calculate conductance from a suitable block of the SMatrix."""

        # Note: Here the conductance is calculated using a norm (the
        # Frobenius-norm, to be precise) In fact, ||A||_F^2=Tr(A A^H), so it's
        # correct.

        if self.r != None:
            cond = self.r.shape[0] - np.linalg.norm(self.r)**2
        elif self.t != None:
            cond = np.linalg.norm(self.t)**2
        elif self.rp != None:
            cond = self.rp.shape[0] - np.linalg.norm(self.rp)**2
        elif self.tp != None:
            cond = np.linalg.norm(self.tp)**2

        return cond

    def noise(self):
        """Calculate noise from a suitable block of the SMatrix.

        Not optimized, possibly could be done a fraction faster.
        """
        if self.r != None:
            sblock = self.r
        elif self.t != None:
            sblock = self.t
        elif self.rp != None:
            sblock = self.rp
        elif self.tp != None:
            sblock = self.tp
        n = sblock.shape[0]
        sbsbdag = sblock * sblock.H
        noise = ml.trace(sbsbdag * (ml.identity(n, dtype=sbsbdag.dtype)
                                    - sbsbdag))
        assert noise.imag == 0 or noise.real > 1000 * noise.imag

        return noise.real


class MMatrix(object):
    """A transfer matrix.

    The conventions used for labelling the sub matrices are taken from section
    1.1.1 of the doctoral thesis of P. W. Brouwer, "On the Random-Matrix Theory
    of Quantum Transport"
    (http://lorentz.leidenuniv.nl/beenakker/theses/brouwer/brouwer.html).

    Transfer matrices can be multiplied together with the multiplication
    operator *, i.e. statements as m21 = m2 * m1 are valid.
    """

    def __init__(self):
        self.m11 = None
        self.m21 = None
        self.m12 = None
        self.m22 = None

    @classmethod
    def from_matrix(cls, mat, nx=None, ny=None):
        """Make a MMatrix from a general matrix.

        mat --- matrix or np.array
        nx, ny --- shape of the reflection block of the s-matrix to make.
        Other blocks are calculated such that the shapes agree.
        If not given, a value is guessed according to a typical situation.
        """
        if nx == None and ny == None:
            nx = mat.shape[0] // 2
            ny = nx
        elif n == None:
            nx = ny
        elif m == None:
            ny = nx
        if type(mat) != np.matrix:
            mat = np.mat(mat)

        m = cls()
        m.m11 = mat[: nx, : ny]
        m.m21 = mat[nx :, : ny]
        m.m12 = mat[: nx, ny :]
        m.m22 = mat[nx :, ny :]
        return m

    @classmethod
    def from_s(cls, s):
        """Make a MMatrix from a SMatrix."""
        m = cls()
        # m11 = t^H^-1 = t - rp * tp^-1 * r
        # m21 = -tp^-1 * r
        # m12 = rp * tp^-1
        # m22 = tp^-1
        m.m22 = s.tp.I
        m.m21 = -m.m22 * s.r
        m.m11 = s.t + s.rp * m.m21
        m.m12 = s.rp * m.m22
        return m

    @classmethod
    def from_tb(cls, h, t, inv_t=None):
        """Make tight-binding M-Matrix from slice Hamiltonian and hopping.

        h     -- Hamiltonian of the slice.
        t     -- hopping to the slice from the left.
        inv_t -- inverse of t (for numerical efficiency), will be calculated
                 from t if None
        """
        n = h.shape[0]
        assert h.shape[1] == n
        assert t.shape == (n, n)

        if inv_t == None:
            inv_t = t.I
        assert inv_t.shape == (n, n)

        # Create blocks of transfer matrix in lattice space.
        m12 = 0.5 * inv_t
        m21 = 0.5 * -t.H
        m22 = 0.5 * -h * inv_t

        # Transform to eigenstates of current operator in infinitely doped
        # leads.
        ret = cls()
        ret.m11 = -1j * m12 + 1j * m21 + m22
        ret.m12 = 1j * m12 + 1j * m21 - m22
        ret.m21 = -1j * m12 - 1j * m21 - m22
        ret.m22 = 1j * m12 - 1j * m21 + m22
        return ret

    def to_matrix(self):
        return np.bmat([[self.m11, self.m12], [self.m21, self.m22]])

    def __mul__(self, b):
        """Return product of two transfer matrices."""
        m = MMatrix()
        m.m11 = self.m11 * b.m11 + self.m12 * b.m21
        m.m12 = self.m11 * b.m12 + self.m12 * b.m22
        m.m21 = self.m21 * b.m11 + self.m22 * b.m21
        m.m22 = self.m21 * b.m12 + self.m22 * b.m22
        return m

    def how_not_conserving(self, norm=None):
        """Check for deviations from current conserving transfer matrix.

        Compute ||sigma_z - M^dagger sigma_z M||, where the kind of norm is
        specified by parameter `norm' which will be passed on to
        scipy.linalg.norm.  The parameter can take, amongst others, the
        following values:

        None -- Frobenius norm (default)
        1    -- 1-norm
        2    -- 2-norm
        inf  -- infinity-norm

        Consult the documentation of scipy.linalg.norm for all possibilities.
        """
        from guts.matrix import sigma_z as s_z
        m = self.to_matrix()
        (l1, l2) = m.shape
        return la.norm(s_z(l2 // 2) - m.H * s_z(l1 // 2) * m, norm)

def combine_generic(s2, s1, blocks=('r', 't', 'rp', 'tp')):
    """Combine two scattering matrices, calculating only specified subblocks

    s2 (the first argument) is the scattering matrix of the RIGHT scattering
    region to be combined.  Consequently, s1 is the scattering matrix of the
    LEFT scattering region.  This order makes sense if you think about
    combining the corresponding transfer matrices m2 and m1 by multiplication:
    m2 * m1.
    """
    # Note: This function implements Eq. 1.1.10 of Brouwer's thesis (see
    # docstring of this module for reference).  That equation contains several
    # typos.  Our version is correct and consistent, as verified by the tests
    # in test_sm.py.

    assert s1.rp != None
    assert s2.r != None
    nr = s1.rp.shape[0]
    nl = s1.rp.shape[1]
    assert s2.r.shape == (nl, nr)
    s = SMatrix()

    if 'r' in blocks or 't' in blocks:
        assert s1.t.shape[0] == nr

        # Note: the "dtype = np.common_type(..)" statement here is needed to
        # keep the precision of S the same as S1 and S2. If it wouldn't be
        # there, the combined matrix would always be promoted to double
        # However, sometimes (to check convergence, for example), it can be
        # useful to keep everything in single precision

        dtype = np.common_type(s1.rp, s2.r)
        temp = la.lu_factor(ml.identity(nr, dtype=dtype) - s1.rp * s2.r)
        temp = la.lu_solve(temp, s1.t)
    if 'r' in blocks:
        assert s1.tp.shape[1] == nl
        s.r = s1.r + s1.tp * s2.r * temp
    if 't' in blocks:
        assert s2.t.shape[1] == nr
        s.t = s2.t * temp

    if 'rp' in blocks or 'tp' in blocks:
        assert s2.tp.shape[0] == nl

        dtype = np.common_type(s2.r, s1.rp)
        temp = la.lu_factor(ml.identity(nl, dtype=dtype) - s2.r * s1.rp)
        temp = la.lu_solve(temp, s2.tp)
    if 'tp' in blocks:
        assert s1.tp.shape[1] == nl
        s.tp = s1.tp * temp
    if 'rp' in blocks:
        assert s2.t.shape[1] == nr
        s.rp = s2.rp + s2.t * s1.rp * temp

    return s

def combine(s2, s1):
    """Shorter notation for full combination of scattering matrices"""
    return combine_generic(s2, s1)

def fast_combine(s2, s1):
    """Combine two scattering matrices calculating only r and t blocks"""
    return combine_generic(s2, s1, ('r', 't'))

def min_combine(s2, s1):
    """Combine two scattering matrices, calculating only the r block"""
    return combine_generic(s2, s1, ('r'))

def get_wave_function(s2, s1):
    """Calculate wave function in the middle of the system

    s2 (the first argument) is the scattering matrix to the right of the
    slice.  where the wave function has to be calculated. Accordingly, s1 is
    the scattering matrix of the region to the left. Current is sent from the
    left lead to the right lead. This function is necessary to calculate
    things like current or particle density distribution

    The basis in which the wave function is given is the current basis, with
    each mode carrying unit current. First half of the modes are right-moving,
    second half is left-moving'
    """
    assert s1.rp != None
    assert s2.r != None
    assert s1.t != None
    n = s1.rp.shape[0]
    assert s1.rp.shape == s2.r.shape == (n, n)
    assert s1.t.shape[1] == n

    psi = np.mat(s1.t.shape[0] * [1]).T
    temp = la.lu_factor(ml.identity(n) - s1.rp * s2.r)
    psi1 = la.lu_solve(temp, s1.t * psi)
    psi2 = s2.r * psi1

    return (psi1, psi2)

def make_lead_s(m, eps=1e-9):
    """Make a lead scattering matrix given a single slice transfer matrix.

    Transfer matrix m corresponds to propagation by one unit cell of the lead
    away from the interface.
    eps --- tolerance for decay length of propagating modes.
    """
    assert isinstance(m, MMatrix), type(m)
    # Check that lead transfer matrix allows translational invariance.
    assert m.m11.shape[0] == m.m11.shape[1]
    assert m.m22.shape[0] == m.m22.shape[1]
    # Calculate eigensystem of transfer matrix.
    nr, nl = m.m11.shape[0], m.m22.shape[0]
    n = nr + nl
    cur = sp.dia_matrix(([nr * [1] + nl * [-1]],
                        [0]), (n, n)).tocsr()
    vals, modes = la.eig(m.to_matrix())
    vals = abs(vals)
    # Select evanescent modes.
    eva = modes[:, np.where(vals < 1 - eps)[0]]
    # Select propagating modes.
    modes2 = np.mat(modes[:, np.where((1 - eps < vals) & (vals < 1 + eps))[0]])
    # Calculate the current operator and its eigensystem.
    if modes2.shape[1] != 0:
        current = modes2.H * cur * modes2
        vals3, modes3 = la.eigh(current)
        norms = sp.dia_matrix(([1/np.sqrt(abs(vals3))], [0]),
                              current.shape).tocsr()
        modes3 = np.mat(modes3) * norms
        # Calculate the outgoing and incoming modes.
        out = modes2 * modes3[:, np.where(vals3 > 0)[0]]
        inc = modes2 * modes3[:, np.where(vals3 < 0)[0]]
        nrlead = out.shape[1]
        nllead = inc.shape[1]
    else:
        out, inc= 2 * (ml.empty((n, 0)),)
        nrlead, nllead = (0, 0)
    s = SMatrix()
    a = np.bmat([[out, eva]])
    ar = a[: nr]
    al = a[nr :]
    arinv = la.lu_factor(ar)
    r1 = la.lu_solve(arinv, np.identity(nr))
    s.t = np.mat(-r1[: nrlead, :])
    s.r = np.mat(al * r1)
    r2 = la.lu_solve(arinv, inc[: nr])
    s.rp = np.mat(r2[: nrlead])
    s.tp = np.mat(inc[nr :] - al * r2)

    return s
