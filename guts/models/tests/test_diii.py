# Copyright 2010 A. R. Akhmerov, C. W. Groth, M. Wimmer.
#
# This file is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.
from __future__ import division
import numpy as np, numpy.matlib as ml
from guts.models import diii
from guts.misc import how_different

def test_for_zero_cond():
    """For theta = 0 and alpha = n*pi/2 the conductance should vanish."""
    from math import pi
    from guts.sm import fast_combine

    alpha = np.array([[3, 1],
                      [3, 3],
                      [1, 3]]) * (pi/2)
    theta = np.zeros((3, 2))
    eta = np.array([[-1,  1],
                    [ 1,  1],
                    [-1, -1]])
    s = reduce(fast_combine, diii.smatrices(alpha, theta, eta))
    assert s.cond() < 1e-10

    # Now it should be non-zero
    theta = np.ones((3, 2))
    s = reduce(fast_combine, diii.smatrices(alpha, theta, eta))
    assert s.cond() > 1e-10

def test_theta_pi_over_2():
    """Test for some special values."""
    from math import cos, sin, pi
    theta = pi / 2

    for e in 1, -1:
        alpha = 0.3
        r, t = cos(alpha), sin(alpha)

        s_should = np.mat([[ 0,    r,    0,   -t],
                           [-r,    0,  t*e,    0],
                           [ 0, -t*e,    0, -r*e],
                           [ t,    0,  r*e,    0]])
        s_is = diii.smatrix_s_slice([alpha], [theta], [e]).to_matrix()
        assert how_different(s_should, s_is) < 1e-15, \
               how_different(s_should, s_is)

        alpha = 0.4
        r, t = cos(alpha), sin(alpha)

        s_should = np.mat([[   0,    0,  r*e,    t],
                           [   0,    0,  t*e,   -r],
                           [-r*e, -t*e,    0,    0],
                           [  -t,    r,    0,    0]])
        s_is = diii.smatrix_sp_slice([alpha], [theta], [-e]).to_matrix()
        assert how_different(s_should, s_is) < 1e-15, \
               how_different(s_should, s_is)

def test_s_sp_equivalence():
    """Check scattering matrices of S- and S'-slices for equivalence."""
    alpha = [0.1, 0.2, 0.3]
    theta = [0.4, 0.5, 0.6]
    eta = [1, -1, -1]
    mineta = [-1, 1, 1]
    n = 2 * len(alpha)

    # Permutation matrix: S' = P * S * P^T
    p = np.mat([[0, 1, 0, 0],
                [0, 0, 0, 1],
                [1, 0, 0, 0],
                [0, 0, 1, 0]])
    n_times_p = ml.zeros((2*n, 2*n))
    identity = ml.identity(n // 2)
    n_times_p[0 : n, 0 : n] = ml.kron(identity, p[0 : 2, 0 : 2])
    n_times_p[n : 2*n, 0 : n] = ml.kron(identity, p[2 : 4, 0 : 2])
    n_times_p[0 : n, n : 2*n] = ml.kron(identity, p[0 : 2, 2 : 4])
    n_times_p[n : 2*n, n : 2*n] = ml.kron(identity, p[2 : 4, 2 : 4])

    # Additionally, the inputs / outputs of S' are shifted.
    shift = ml.zeros((2*n, 2*n), dtype=np.int)
    for i in range(n):
        j = (i + 1) % n
        shift[i, j] = 1
        shift[i+n, j+n] = 1

    s = diii.smatrix_s_slice(alpha, theta, mineta).to_matrix()
    sp_should = shift * n_times_p * s * n_times_p.T * shift.T
    sp_is = diii.smatrix_sp_slice(alpha, theta, eta).to_matrix()
    assert how_different(sp_should, sp_is) < 1e-15, \
           how_different(sp_should, sp_is)

def test_unitarity():
    """Check unitarity of the scattering matrix."""
    alpha = [0.4, 0.3, 0.2]
    theta = [0.1, 0.3, 0.5]
    eta = [-1, -1, 1]
    s = diii.smatrix_s_slice(alpha, theta, eta)
    assert s.how_nonunitary() < 1e-15
