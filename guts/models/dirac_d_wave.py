# Copyright 2010 A. R. Akhmerov, C. W. Groth, M. Wimmer.
#
# This file is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.
"""Momentum space discretization of Dirac fermions in a d-wave superconductor.

Notations follow the draft in Dropbox.
"""

from __future__ import division
import numpy as np, numpy.matlib as ml
from scipy import sparse as sp
from guts import sm, misc, matrix, continuum_transport as ct

s_0 = ml.identity(2)
s_z = matrix.sigma_z(1, False)
s_x = matrix.sigma_x(1, False)
s_y = matrix.sigma_y(1, False)
kron = matrix.kron

def prepare_hamiltonian(alpha, aniso):
    """Precalculate parts of the Hamiltonian in the current basis.

    alpha --- angle between interfaces and crystallographic axes,
    aniso --- ratio v_delta/v_f.
    fermi velocity v_f is set to 1. To avoid the need to sort valleys, alpha
    has to be from 0 to pi/2
    """

    # Creating parts of the Hamiltonian in lattice coordinates. In kronecker
    # products the first pauli matrix selects between time-reversed pairs of
    # nodes (A, C) and (B, D), the second one selects the valley inside a pair,
    # and finally the third one selects electrons and holes.

    # Defining various matrices
    dh = {}
    # Current along crystallographic axes
    dh['i_x'] = kron((s_0 + s_z)/2, s_z, s_z) + \
                aniso * kron((s_0 - s_z)/2, s_z, s_x)
    dh['i_y'] = kron((s_0 - s_z)/2, s_z, s_z) + \
                aniso * kron((s_0 + s_z)/2, s_z, s_x)
    # Current along sample axes
    dh['i_s'] = dh['i_y'] * np.sin(alpha) + dh['i_x'] * np.cos(alpha)
    dh['i_t'] = dh['i_y'] * np.cos(alpha) - dh['i_x'] * np.sin(alpha)
    # Current in each valley
    dh['i_a'] = dh['i_s'] * kron((s_0 + s_z)/2, (s_0 + s_z)/2, s_0)
    dh['i_b'] = dh['i_s'] * kron((s_0 - s_z)/2, (s_0 + s_z)/2, s_0)
    dh['i_c'] = dh['i_s'] * kron((s_0 + s_z)/2, (s_0 - s_z)/2, s_0)
    dh['i_d'] = dh['i_s'] * kron((s_0 - s_z)/2, (s_0 - s_z)/2, s_0)
    # Chemical potential and mixing of the valleys
    dh['mu_0'] = kron(s_0, s_0, s_z)
    dh['mu_ac'] = kron((s_0 + s_z)/2, s_x, s_z)
    dh['mu_bd'] = kron((s_0 - s_z)/2, s_x, s_z)
    dh['mu_ab_cd'] = kron(s_x, s_x, s_z)
    dh['mu_ad_bc'] = kron(s_x, s_0, s_z)
    dh['en'] = kron(s_0, s_0, s_0)

    t = ct.find_modes(dh['i_s'])
    dh['t'] = t
    for i in dh:
        dh[i] = t.H * dh[i] * t
    for i in dh:
        dh[i] = np.mat(np.round(dh[i], 13))
        if i != 't':
            assert misc.how_different(dh[i], dh[i].H) < 1e-13
    assert misc.how_different(dh['i_s'], matrix.sigma_z(4, False)) < 1e-13
    dh['i_s'] = matrix.sigma_z(4, False)
    return dh

def s_free(n, inv_asp, dh, pot):
    q = [2 * np.pi * dh['i_t'] * inv_asp * i for i in range(-(n//2), n//2 + 1)]
    v = 0
    for i in pot:
        v += pot[i] * dh[i]
    q2 = [i + v for i in q]
    s = ct.smatrix_free(q2)

    return s

def s_slice(inv_asp, dh, pot, qp):
    """Generate a slice scattering matrix.

    dh --- dictionary containing matrix structure of all the Hamiltonian
    parameters,
    pot --- dictionary with positive Fourier components of different kinds of
    disorder,
    qp --- order of Pade approximation to transfer matrix to use.
    """
    n = pot[pot.keys()[0]].shape[0]
    assert n%2 == 1
    mat_v = np.mat(np.zeros((n, n)))
    h2 = np.mat(np.zeros((8*n, 8*n)))
    for i in pot:
        for q1 in range(0, n):
            for q2 in range(q1 + 1, n):
                mat_v[q1, q2] = pot[i][q2 - q1 - 1]
        h = np.bmat(mat_v)
        h1 = h + h.H
        h1 += np.diag(n * [pot[i][0]])
        h2 += kron(h1, dh[i])
    q = [2 * np.pi * inv_asp * i for i in range(-(n//2), n//2 + 1)]
    h2 += kron(np.diag(q), dh['i_t'])
    h3 = ct.v_to_current(h2, n * [np.identity(8)])
    s = ct.smatrix_mixing(h3, qp)

    return s

def tun_bar(gamma, n, mu_0):
    """Generate a scattering matrix of a tunnel barrier of strength gamma.

    n --- number of positive transverse momenta taken,
    mu_0 --- matrix corresponding to chemical potential in current basis.
    """
    n = (2*n + 1)
    gamma_eff = 0.5 * np.log(2/gamma - 1)
    s = ct.smatrix_free(n * [gamma_eff * mu_0])
    return s

def el_cond(t, g_l, g_r, dh):
    """Calculate electric conductance from a transmission block of s.

    g_l --- tunnel barrier strength at the left,
    g_r --- tunnel barrier strength at the right,
    dh --- dictionary of Hamiltonian parameters.
    Only the charge conversion factors are taken into account in this function,
    the scattering effects have to be included into calculation of t.
    """
    n = t.shape[0]//4
    l_ab = (1 / (2 - g_l))
    l_cd = ((1 - g_l) / (2 - g_l))
    ll_ab =  l_ab * (dh['i_a'] + dh['i_b'])[: 4, : 4]
    ll_cd =  l_cd * (dh['i_c'] + dh['i_d'])[: 4, : 4]
    blocks = [n * [None] for i in range(n)]
    for i in xrange(n):
        blocks[i][i] = sp.csr_matrix(ll_ab + ll_cd)
    q_l = sp.bmat(blocks, 'csr')
    r_ab = (g_r / (2 - g_r))
    r_cd = -(g_r / (2 - g_r))
    rr_ab =  r_ab * (dh['i_a'] + dh['i_b'])[: 4, : 4]
    rr_cd =  r_cd * (dh['i_c'] + dh['i_d'])[: 4, : 4]
    for i in xrange(n):
        blocks[i][i] = sp.csr_matrix(rr_ab + rr_cd)
    q_r = sp.bmat(blocks, 'csr')
    cond = (q_r * abs(np.array(t) ** 2) * q_l).sum()

    return cond


def smatrices(inv_asp, alpha, aniso, pot, q):
    """Generate the scattering matrices for a given system.

    inv_asp --- inverse aspect ratio,
    alpha --- angle between interfaces and crystallographic axes,
    aniso --- ratio v_delta/v_f,
    pot --- dictionary of two-dimensional arrays setting the strength of
    various disorders,
    q --- order of Pade approximation to transfer matrix to use.
    Number of modes and number of slices are calculated from dimensions of
    entries of pot.
    The scattering matrices are generated from right to left to allow combining
    them with guts.sm.fast_combine.
    """

    dh = prepare_hamiltonian(alpha, aniso)
    (w, l) = pot[pot.keys()[0]].shape
    inv_asp /= l
    pot_slice = dict()
    pot_0 = dict()

    # Calculating scattering matrices slice by slice
    for x in reversed(range(l)):
        for i in pot:
            pot_slice[i] = pot[i][:, x] / l
            pot_0[i] = pot[i][0, x] / l
        s = s_slice(inv_asp, dh, pot_slice, q)
        yield s
